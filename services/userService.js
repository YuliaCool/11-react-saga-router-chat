const { UserRepository } = require('../repositories/userRepository');
const defaultAvatar = require('../config/defaultAvatar');

class UserService {

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    add(item) {
        const success = UserRepository.create(item);
        if(!success){
            return null;
        }
        return success;
    }

    update(id, updateData) {
        const item = UserRepository.getOne({id});
        if(!item) {
            return null;
        }
        else{
            const updated = UserRepository.update(id, updateData);
            if(!updated) {
                return null;
            }
            return updated;
        }
        
    }

    getAll(){
        const users = UserRepository.getAll();
        if(!users){
            return null;
        }
        return users;
    }

    getOneById(id){
        const success = UserRepository.getOne(id);
        if(!success){
            return null;
        }
        return success;
    }

    delete(id){
        const item = UserRepository.getOne({id});
        if(!item) {
            return null;
        }
        else{
            const deleted = UserRepository.delete(id)
            if(!deleted) {
                return null;
            }
            return deleted;
        } 
    }

    getUsers() {
        const users = UserRepository.getAll();
        if(!users) {
            throw new NotFound("Users are not found");
        }
        return users;
    }

    getUser(id) {
        const user = this.search({ id });
        if(!user) {
            throw new NotFound("User is not found");
        }
        return user;
    }

    createUser(userData) {
        const { password, name } = userData;
        let { email, avatar } = userData;
        email = email.toLowerCase();
        if (avatar === "")
            avatar = defaultAvatar;
        const uniqueLogin = this.checkUniqueLogin(email);
        if (!uniqueLogin) {
           throw new Error ("Database has already consisted user with this email");
        }
        const createdUser = UserRepository.create({ email, password, name, avatar });
        return createdUser;
    }

    updateUser(id, userData) {
        const user = this.search({ id });
        if (!user) {
            throw new NotFound("User is not found");
        }

        const { password, name } = userData;
        let { email, avatar } = userData;
        email = email.toLowerCase();
        if (avatar === "") {
            avatar = defaultAvatar;
        }

        const uniqueLogin = this.checkUniqueLogin(email);
        if (!uniqueLogin) {
            const userWithSameLogin = this.getUserByLogin(email);
            if(userWithSameLogin.id !== id)
                throw new Error ("Database has already consisted user with this email");
        }
        const updatedUser = UserRepository.update(id, {email, password, name, avatar});
        return updatedUser;
    }

    deleteUser(id) {
        const user = this.search({ id });
        if (!user) {
            throw new NotFound("User is not found");
        }
        const deletedUser = UserRepository.delete(id);
        return deletedUser;
    }

    checkUniqueLogin(email) {
        const userWithSameLogin = this.search({ email });
        if (userWithSameLogin)
            return false;
        else return true;
    }
    getUserByLogin(email) {
        return this.search({ email });
    }
}

module.exports = new UserService();