const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {

    search(search) {
        const item = MessageRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    add(item) {
        const success = MessageRepository.create(item);
        if(!success){
            return null;
        }
        return success;
    }

    update(id, updateData) {
        const message = MessageRepository.getOne({id});
        if(!message) {
            return null;
        }
        else{
            const updatedMessage = MessageRepository.update(id, updateData);
            if(!updatedMessage) {
                return null;
            }
            return updatedMessage;
        }
        
    }

    getAll(){
        const messages = MessageRepository.getAll();
        if(!messages){
            return null;
        }
        return messages;
    }

    getOneById(id){
        const message = MessageRepository.getOne(id);
        if(!message){
            return null;
        }
        return message;
    }

    delete(id){
        const message = MessageRepository.getOne({id});
        if(!message) {
            return null;
        }
        else{
            const deletedMessage = MessageRepository.delete(id)
            if(!deletedMessage) {
                return null;
            }
            return deletedMessage;
        } 
    }

    getMessages() {
        const messages = MessageRepository.getAll();
        if(!messages) {
            throw new NotFound("Messages are not found");
        }
        return messages;
    }

    getMessage(id) {
        const message = this.search({ id });
        if(!message) {
            throw new NotFound("Message is not found");
        }
        return message;
    }

    createMessage(messageData) {
        const createdMessage = MessageRepository.create(messageData);
        return createdMessage;
    }

    updateMessage(id, messageData) {
        const message = this.search({ id });
        if (!message) {
            throw new NotFound("Message is not found");
        }
        const updatedMessage = MessageRepository.update(id, messageData);
        return updatedMessage;
    }

    deleteMessage(id) {
        const message = this.search({ id });
        if (!message) {
            throw new NotFound("Message is not found");
        }
        const deletedMessage = MessageRepository.delete(id);
        return deletedMessage;
    }

    sortMessagesByCreatedDate(messages) {
        messages.sort((first, second) => {
            return new Date(first.createdAt) - new Date(second.createdAt);
        });
        return messages;
    }
}

module.exports = new MessageService();