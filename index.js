const express = require('express');
const cors = require('cors');
const app = express(); 
//const router = express.Router(); 

const port = 8080; 

app.use(cors());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

//app.use('/api', router.rout); 

app.listen(port, function () {
    console.log('app running on port ' + port);
});

exports.app = app;