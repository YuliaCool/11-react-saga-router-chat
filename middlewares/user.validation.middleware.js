const checkUserValidData = (req, res, next) => {
    try {
        const { email, password } = req.body;
        if(!email){
            throw new BadRequest("Empty email");
        }
        if(!password){
            throw new BadRequest("Empty password");
        }
        if(!validateEmail(email)) {
            throw new BadRequest("Invalid email");
        }
    } catch(err) {
        res.err = err;
    } finally {
        next();
    }
};

function validateEmail(email){
    const emailPattern = /^\w+([\.-]?\w+)*@gmail+\.[a-zA-Z]{2,4}$/; 
    return emailPattern.test(email);
}

module.exports = checkUserValidData;