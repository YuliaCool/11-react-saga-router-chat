import { combineReducers } from 'redux';
import chat from '../containers/Chat/reducer';
import currentUser from '../containers/Chat/CurrentUser/reducer';
import updateMessagePage from '../containers/UpdateMessagePage/reducer';
import usersList from '../containers/UsersList/reducer';
import userPage from '../containers/UserPage/reducer';

const rootReducer = combineReducers({
    chat,
    currentUser,
    usersList,
    userPage,
    updateMessagePage
});

export default rootReducer;