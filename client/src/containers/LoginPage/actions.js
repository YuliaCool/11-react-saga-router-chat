
import { login, setLoginSession, unsetLoginSession } from '../../services/authService';
import { adminEmail } from '../../helpers/constants';

export let isAuthenticated = false;
export let isAdmin =  false;

export const authenticate = async ({ email, password }) => {
    const data = await login({ email, password });
    if(data && !data.error) {
        isAuthenticated = true;
        setLoginSession(data);
        if(data.email === adminEmail) {
            isAdmin = true;
        } 
    }
    return data;
};

export const signout = () => {
    isAuthenticated = false;
    isAdmin = false;
    unsetLoginSession();
}
