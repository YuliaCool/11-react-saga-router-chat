import React, { useState } from 'react';
import { useLocation } from "react-router-dom";
import * as Authentification from './actions';
import './styles.css';

export const LoginPage = () => {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    const onEmailChange = (event) => {
        setEmail(event.target.value);
    }

    const onPasswordChange = (event) => {
        setPassword(event.target.value);
    }

    const location = useLocation();

    let { from } = location.state || { from: { pathname: "/" } };

    const onLogin = () => {
        Authentification.authenticate({ email, password })
        /*Authentification part: set values of Auth
        .then( (data) => {
            if (Authentification.isAdmin)
               history.push('/userslist');
            else if (Authentification.isAuthenticated) 
                history.push('/chat');
            else alert (data.message);

        });*/
    };

    return (
        <div className="login-container">
            <p>You must log in to view the page at {from.pathname}</p>
            <input className="login-input" type="text" name="email" onChange={onEmailChange} placeholder="login" />
            <input className="login-input" type="password" name="password" onChange={onPasswordChange} placeholder="password" />
            <button onClick={onLogin}>Log in</button>
        </div>
    );
}
