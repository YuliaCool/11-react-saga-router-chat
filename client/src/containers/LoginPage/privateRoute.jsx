import React, { Component } from 'react';
import { Route, Redirect } from "react-router-dom";
import * as Authentification from './actions';

export const PrivateRoute = ({ children, ...rest }) => {
    return (
      <Route
        {...rest}
        render={({ location }) =>
        Authentification.isAdmin ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }