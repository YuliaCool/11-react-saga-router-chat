import * as messageService from '../../services/messageService';
import { v4 as uuidv4 } from 'uuid';

export const loadMessages = async () => {
    const allMessages = await messageService.getAllMessages();
    return allMessages;
}

export const countParticipants = (allMessages) => {
    return messageService.getCountParticipants(allMessages);
}

export const countMessages = allMessages => {
    return allMessages.length;
}

export const updateMessageLastCreated = allMessages => {
    const lastCreatedMessage = findLastMessage(allMessages);
    return `${new Date(lastCreatedMessage.createdAt).toLocaleString()}`;
}

export const findLastMessage = (messages) => {
    const mostRecentDate = new Date(Math.max.apply(null, messages.map( messageItem => {
        return new Date(messageItem.createdAt);
     })));
     const mostRecentMessage = messages.filter( messageItem => { 
         const date = new Date( messageItem.createdAt ); 
         return date.getTime() === mostRecentDate.getTime();
     })[0];
     return mostRecentMessage;
}


export const findLastMessageByUser = (messages, user) => {
    const userMessages = filterMessagesByUser(messages, user);
    const mostRecentUserMessage = findLastMessage(userMessages);
    return mostRecentUserMessage;
}

const filterMessagesByUser = (messages, user) => {
    const userMessages = messages.filter(message => message.userId === user.id);
    return userMessages;
}


export const getNewId = () => {
    return uuidv4();
}

