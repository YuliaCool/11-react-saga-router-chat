import { SET_CURRENT_USER } from './actionTypes';

export const setCurrentUser = data => ({
    type: SET_CURRENT_USER,
    payload: {
        data
    }
});
