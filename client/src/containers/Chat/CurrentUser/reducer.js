import { SET_CURRENT_USER } from './actionTypes';

const initialState = {
        id: "533b5230-1b8f-11e8-9629-c7eca82aa7bd",
        name: "Wendy",
        avatar: "https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cjs0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng"
    };

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_USER: {
            const { id, data } = action.payload;
            const currentUser = { id, ...data }
            return [...state, currentUser];
        }
        default:
           return state;
    }
    
}
