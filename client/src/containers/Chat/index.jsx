import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import store from '../../store/configureStore';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import * as service from './service';
import ChatHeader from '../../components/ChatHeader/ChatHeader';
import MessageList from '../../components/MessageList/MessageList';
import AddMessage from '../../components/AddMessage';
import Spinner from '../../components/Spinner';

import './styles.css';

class Chat extends Component {
    constructor(props) {
        super(props);

        this.onAddMessage = this.onAddMessage.bind(this);
        this.onDeleteMessage = this.onDeleteMessage.bind(this);
        this.onEditMessage = this.onEditMessage.bind(this);
        this.likeMessage = this.likeMessage.bind(this);
        this.keyEditMessagePress = this.keyEditMessagePress.bind(this);
    }

    componentDidMount() {
        this.props.fetchMessages();   
        document.addEventListener("keydown", this.keyEditMessagePress, false);
    } 
    componentWillUnmount(){
        document.removeEventListener("keydown", this.keyEditMessagePress, false);
    }

    onAddMessage(text) {
        this.props.addMessage(text, this.props.currentUser);
    }

    onDeleteMessage(id) {
        this.props.deleteMessage(id);
    }
    onEditMessage(id) {
        this.props.history.push(`/messages/${id}`);
    }

    keyEditMessagePress(e) {
        if(e.code === 'ArrowUp') {
            const lastUserUpdatedMessage = service.findLastMessageByUser(this.props.messages, this.props.currentUser);
            this.onEditMessage(lastUserUpdatedMessage.id);
        }
    }

    likeMessage(id, likedUserId, key) {
        store.dispatch(actions.likeMessage(id, likedUserId, key));
    }

    renderChat() {
        return (
        <div className="chat-container">
            <ChatHeader 
                messages={this.props.messages}
            /> 
            <MessageList 
                messages={this.props.messages} 
                currentUserId={this.props.currentUserId}
                onDelete={this.onDeleteMessage} 
                onEdit={this.onEditMessage}
                onLike={this.likeMessage}
            />
            <AddMessage addMessageHandler = { this.onAddMessage } />
        </div>
        );
    };

    render() {
        return (
            <div>
                {
                    this.props.isLoaded 
                    ?  this.renderChat()
                    :  <Spinner /> 
                }
            </div>
        );
    }
};

Chat.propTypes = {
    userData: PropTypes.object
}

const mapStateToProps = (state) => {
    return { 
        messages: state.chat.messages,
        isLoaded: state.chat.isLoaded,
        currentUserId: state.currentUser.id,
        currentUser: state.currentUser
    }
};

const allActions = {
    ...actions
};

const mapDispatchToProps = dispatch => bindActionCreators(allActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);