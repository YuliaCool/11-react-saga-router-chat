import { FETCH_MESSAGES_SUCCESS, FETCH_MESSAGES_ERROR, FETCH_MESSAGES_START } from "./actionTypes";

export default function (state = [], action, isLoaded=false, errors={}) {
    switch (action.type) {
        case FETCH_MESSAGES_START: {
            return { isLoaded: action.payload.isLoaded };
        }
        case FETCH_MESSAGES_SUCCESS: {
            return { isLoaded: action.payload.isLoaded, messages: [...action.payload.messages] };
        }
        case FETCH_MESSAGES_ERROR: {
            return { isLoaded: action.payload.isLoaded, errors: [...action.payload.errors] };
        }
        default:
            return state;
    }
}
