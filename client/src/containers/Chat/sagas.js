import axios from 'axios';
import { apiUrl } from '../../helpers/constants';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_NEW_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES } from "./actionTypes";

export function* fetchMessages() {
	try {
        yield put({ type: 'FETCH_MESSAGES_START', payload: { isLoaded: false} })
        const messages = yield call(axios.get, `${apiUrl}/api/messages`);
		yield put({ type: 'FETCH_MESSAGES_SUCCESS', payload: { messages: messages.data, isLoaded: true } })
	} catch (error) {
        yield put({ type: 'FETCH_MESSAGES_ERROR', payload: { errors: error.message, isLoaded: true } })
	}
}

function* watchFetchMessage() {
	yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* addMessage(action) {
	const newMessage = { ...action.payload };
	try {
		yield call(axios.post, `${apiUrl}/api/messages`, newMessage);
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
        yield put({ type: 'FETCH_MESSAGES_ERROR', payload: { errors: error.message, isLoaded: true } })
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_NEW_MESSAGE, addMessage)
}

export function* updateMessageSaga(action) {
	const id = action.payload.id;
    const updatedMessage = { text: action.payload.text };
	
	try {
		yield call(axios.put, `${apiUrl}/api/messages/${id}`, updatedMessage);
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
		yield put({ type: 'FETCH_MESSAGES_ERROR', payload: { errors: error.message, isLoaded: true } })
	}
}

function* watchUpdateMessage() {
	yield takeEvery(UPDATE_MESSAGE, updateMessageSaga)
}

export function* deleteMessage(action) {
	try {
		yield call(axios.delete, `${apiUrl}/api/messages/${action.payload.id}`);
		yield put({ type: FETCH_MESSAGES })
	} catch (error) {
		yield put({ type: 'FETCH_MESSAGES_ERROR', payload: { errors: error.message, isLoaded: true } })
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* messagesSagas() {
	yield all([
		watchFetchMessage(),
		watchAddMessage(),
		watchUpdateMessage(),
		watchDeleteMessage()
	])
};
