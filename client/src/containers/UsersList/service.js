
import { v4 as uuidv4 } from 'uuid';

const getNewId = () => {
    return uuidv4();
}


export default {
    getNewId
};