import { FETCH_USERS_SUCCESS, FETCH_USERS_ERROR, FETCH_USERS_START } from "./actionTypes";

export default function (state = [], action, isLoading=false, errors={}) {
    switch (action.type) {
        case FETCH_USERS_START: {
            return { isLoading: action.payload.isLoading };
        }
        case FETCH_USERS_SUCCESS: {
            return { isLoading: action.payload.isLoading, users: [...action.payload.users] };
        }
        case FETCH_USERS_ERROR: {
            return { isLoading: action.payload.isLoading, users: [...action.payload.errors] };
        }
        default:
            return state;
    }
}
