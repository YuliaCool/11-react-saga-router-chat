import React, { Component } from 'react';
import UserItem from '../../components/UserItem';
import Spinner from '../../components/Spinner';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import PropTypes from 'prop-types';
import './styles.css';

class UsersList extends Component {
    constructor(props) {
		super(props);
		this.onEdit = this.onEdit.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onAdd = this.onAdd.bind(this);
    }

    componentDidMount() {
        this.props.fetchUsers();
	}

    onEdit(id) {
		this.props.history.push(`/users/${id}`);
	}

	onDelete(id) {
		this.props.deleteUser(id);
	}

	onAdd() {
		this.props.history.push('/user');
    }
    
    renderList() {
       return (
        <div>
        {
            this.props.users
            ? <div className="users-list-container">
            {
                this.props.users.map(user => {
                    return (
                        <UserItem 
                        key={user.id}
                        id={user.id}
                        avatar={user.avatar}
                        name={user.name}
                        email={user.email}
                        onEdit={this.onEdit}
                        onDelete={this.onDelete}
                        />
                    );
                })
            } </div>
            : null
        }
        </div>
        );
    };

    render() {
        return (
            <div>
                {
                    this.props.isLoading
                    ? <Spinner /> 
                    : this.renderList()
                }
            </div>
        )
    }
    
    // Authentification part: Render page function
    /*render () {  
        const isLogin = isSignedIn();
        return (
                isLogin
                ? this.renderPage()
                : this.props.history.push('/login')
        )
    }*/
}

UsersList.propTypes = {
   userData: PropTypes.object
};

const mapStateToProps = (state) => {
	return {
        users: state.usersList.users,
        isLoading:  state.usersList.isLoading
	}
};

const allActions = {
	...actions
};

const mapDispatchToProps = dispatch => bindActionCreators(allActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
