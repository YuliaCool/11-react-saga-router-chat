import axios from 'axios';
import { apiUrl } from '../../helpers/constants';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_MESSAGE, FETCH_MESSAGE_SUCCESS, FETCH_MESSAGE_ERROR } from "./actionsType";

export function* fetchMessage(action) { 
    try {
        const message = yield call(axios.get, `${apiUrl}/api/messages/${action.payload.id}`);
        yield put({ type: FETCH_MESSAGE_SUCCESS, payload: { messageData: message.data } })
    } catch (error) {
        yield put({ type: FETCH_MESSAGE_ERROR, payload: { errors: error.message } })
    }
}

function* watchFetchMessage() {
    yield takeEvery(FETCH_MESSAGE, fetchMessage)
}

export default function* messagePageSagas() {
    yield all([
        watchFetchMessage()
    ])
};
