import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import { updateMessage } from '../Chat/actions';
import PropTypes from 'prop-types';

import TextInput from '../../components/TextInput';
import './styles.css';

export class UpdateMessagePage extends Component {

    constructor(props){
        super(props);

        this.state = {
            text: ""
        };
        this.onCancel = this.onCancel.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }
    componentDidMount(){
        if (this.props.match.params.id) {
            this.props.fetchMessage(this.props.match.params.id);
            this.setState({ text: this.props.messageData.text });
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.messageData.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.messageData
            };
        } else {
            return null;
        }
    }

    onChangeData(e) {
        const inputedValue = e.target.value;
        this.setState({ text: inputedValue });
    }

    onCancel = () => {
        this.setState({ text: "" });
        this.props.history.push('/chat');
    }

    onEdit = () => {
        this.props.updateMessage(this.state.id, this.state.text);
        this.setState({ text: "" });
        this.props.history.push('/chat');
    };

    
    render(){
        const componentHeader = "Edit message";
        const OkButtonValue = "Ok";
        const CancelButtonValue = "Cancel";
        const text = this.state.text;

        return (
            <div className="update-message-container">
                <div className="container-header">
                    { componentHeader}
                </div>
               <TextInput text={text} onChange={this.onChangeData} />
                <div className="button-container">
                    <button type="submit" onClick={this.onEdit}>{ OkButtonValue }</button>
                    <button type="submit" onClick={this.onCancel}>{ CancelButtonValue }</button>
                </div>
            </div> 
        );
    }
};

UpdateMessagePage.propTypes = {
    messageData: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        messageData: state.updateMessagePage.messageData
    }
}

const allActions = {
    ...actions,
    updateMessage
}
const mapDispatchToProps = dispatch => bindActionCreators(allActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UpdateMessagePage);