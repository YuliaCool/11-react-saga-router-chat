import { FETCH_MESSAGE } from "./actionsType";

export const fetchMessage = id => ({
    type: FETCH_MESSAGE,
    payload: {
        id
    }
});