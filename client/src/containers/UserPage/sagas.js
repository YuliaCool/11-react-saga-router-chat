import axios from 'axios';
import { apiUrl } from '../../helpers/constants';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_USER, FETCH_USER_SUCCESS, FETCH_USER_ERROR } from "./actionTypes";

export function* fetchUser(action) {
    try {
        const user = yield call(axios.get, `${apiUrl}/api/users/${action.payload.id}`);
        yield put({ type: FETCH_USER_SUCCESS, payload: { userData: user.data } })
    } catch (error) {
        yield put({ type: FETCH_USER_ERROR, payload: { errors: error.message } })
    }
}

function* watchFetchUser() {
    yield takeEvery(FETCH_USER, fetchUser)
}

export default function* userPageSagas() {
    yield all([
        watchFetchUser()
    ])
};
