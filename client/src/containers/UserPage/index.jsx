import React, { Component } from "react";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import * as actions from './actions';
import { addUser, updateUser } from '../UsersList/actions';
import FieldInput from '../../components/FieldInput';
import PasswordInput from '../../components/PasswordInput/PasswordInput';
import EmailInput from '../../components/EmailInput/EmailInput';
import { defaultUserConfig } from '../../helpers/constants';
import { userFormConfig } from '../../helpers/constants';
import PropTypes from 'prop-types';
import './styles.css';
const EMAIL = "email";

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultUserData();
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchUser(this.props.match.params.id)
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.userData.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.userData
            };
        } else {
            return null;
        }
    }

    onCancel() {
        this.setState(this.getDefaultUserData());
        this.props.history.push('/userslist');
    }

    onSave() {
        if (this.state.id) {
            this.props.updateUser(this.state.id, this.state);
        } else {
            this.props.addUser(this.state);
        }
        this.setState(this.getDefaultUserData());
        this.props.history.push('/userslist');
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }
        );
    }

    getDefaultUserData() {
        return {
            ...defaultUserConfig
        };
    }

    getInput(data, { label, type, keyword }, index) {
        switch (type) {
            case 'text':
                return (
                    <FieldInput
                        key={index}
                        label={label}
                        text={data[keyword]}
                        keyword={keyword}
                        onChange={this.onChangeData}
                    />
                );
            case 'email':
                return (
                    <EmailInput
                        key={index}
                        label={label}
                        text={data[keyword]}
                        keyword={keyword}
                        ref={EMAIL}
                        onChange={this.onChangeData}
                    />
                );
            case 'password':
                return (
                    <PasswordInput
                        key={index}
                        label={label}
                        type={type}
                        text={data[keyword]}
                        keyword={keyword}
                        onChange={this.onChangeData}
                    />
                );
            default:
                return null;
        }
    }

    render() {
        const data = this.state;

        return (
            <div className="modal" tabIndex="-1" role="dialog">
                <div role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Edit user</h5>
                        </div>
                        <div className="modal-body">
                            {
                                userFormConfig.map((item, index) => this.getInput(data, item, index))
                            }
                        </div>
                        <div className="modal-footer">
                            <button onClick={this.onCancel}>Cancel</button>
                            <button onClick={this.onSave}>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

UserPage.propTypes = {
    userData: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        userData: state.userPage.userData
    }
};

const allActions = {
    ...actions,
    addUser,
    updateUser
};
const mapDispatchToProps = dispatch => bindActionCreators(allActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);