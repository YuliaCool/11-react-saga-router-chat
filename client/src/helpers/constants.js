export const adminEmail = 'admin@gmail.com';
export const apiUrl = "http://localhost:8080";
export const LOGO_URL = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/512px-Telegram_logo.svg.png";
export const defaultUserConfig = {
    "id": "",
    "avatar": "",
    "name": "",
    "email": "",
    "password": ""
}
export const userFormConfig = [
    {
        "label": "Name",
        "type": "text",
        "keyword": "name"
    }, {
        "label": "Email",
        "type": "email",
        "keyword": "email"
    }, {
        "label": "Password",
        "type": "password",
        "keyword": "password"
    }, {
        "label": "Avatar",
        "type": "text",
        "keyword": "avatar"
    }
]