import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class TextInput extends Component {  
    render(){
        return (
            <textarea className="message-input"
                value={this.props.text}
                placeholder="Message"
                onChange={ ev => this.props.onChange(ev) }
            >
            </textarea>
        );
    }
};

TextInput.propTypes = {
    text: PropTypes.string,
    onChange: PropTypes.func
};

export default TextInput;