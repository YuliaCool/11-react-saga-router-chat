import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';
import Messsage from '../Message/Message';

export class MessageList extends Component {
    render() {
        const { messages, currentUserId, onDelete, onEdit, onLike } = this.props;

        return (
            <div className="message-list-container">
                {
                    messages 
                    ? messages.map( message => (
                        <Messsage
                            currentUserId={currentUserId}
                            message={message}
                            key={message.id}
                            onDelete={onDelete} // Maybe it better to use Context here? But level of hierarchy is low  
                            onEdit={onEdit}     // and can we add to Context function? 
                            onLike={onLike}
                        /> 
                        )
                    )
                    : null
                }
            </div>
        );
    }
};

MessageList.propTypes = {
    messages: PropTypes.array,
    currentUserId: PropTypes.string, 
    onDelete: PropTypes.func,
    onEdit: PropTypes.func,
    onLike: PropTypes.func
}

export default MessageList;