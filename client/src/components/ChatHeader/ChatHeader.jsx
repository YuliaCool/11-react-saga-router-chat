import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { countParticipants, countMessages, updateMessageLastCreated } from '../../containers/Chat/service';
import './styles.css';

export class ChatHeader extends Component {
    render() {
        const messages = this.props.messages;
        const participantCount = countParticipants(messages);
        const messagesCount = countMessages(messages);
        const messageLastCreated = updateMessageLastCreated(messages);
        const chatName = "My chat";

        return (
            <div className="header-container">
                <div className="left-part">
                    <div>{chatName}</div>
                    <div>
                        <span>{participantCount}</span>
                        <span> participants</span>
                    </div>
                    <div>
                        <span>{messagesCount}</span>
                        <span> messages</span>
                    </div>
                </div>
                <div className="right-part">
                    <div>
                        <span>last message at </span>
                        <span>{messageLastCreated}</span>
                    </div>   
                </div>
            </div>
        );
    }
};
ChatHeader.propTypes = {
    messages: PropTypes.array
}
export default ChatHeader;