import React, { Component } from 'react';
import './styles.css';

export class Spinner extends Component {
    render() {
        return (
            <div>
                <div className="loader-container">
                    <div className="loader">
                        <span id="one"></span>
                        <span id="two"></span>
                        <span id="three"></span>
                        <span id="four"></span>
                    </div>
                </div>
                <div className="chat-container"></div>
            </div>
        );
    };
}

export default Spinner;