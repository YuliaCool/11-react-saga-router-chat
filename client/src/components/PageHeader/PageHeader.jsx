import React, { Component } from 'react';
import { AuthButton } from '../AuthentificationInfoContainer/authButton';
import  { LOGO_URL } from '../../helpers/constants';
import './styles.css';

export class PageHeader extends Component {
    render() {
        return (
            <div className="header">
                <img src={LOGO_URL} title="telegram" alt="logo" />
                <AuthButton />
            </div>
        );
    }
};

export default PageHeader;