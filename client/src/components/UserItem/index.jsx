import React, { Component } from "react";
import './styles.css';

export default class UserItem extends Component {
    render() {
        const { id, avatar, name, email } = this.props;
        return (
            <div className="list-group-item">
                <div> <img src={avatar} alt={name} title={name} /></div>
                <div className="user-info-container">
                    <div>
                        <div>{name}</div>
                        <div>{email}</div>
                    </div>
                    <div>
                        <button onClick={(e) => this.props.onEdit(id)}> Edit </button>
                        <button onClick={(e) => this.props.onDelete(id)}> Delete </button>
                    </div>
                </div>
            </div>
        );
    }
};