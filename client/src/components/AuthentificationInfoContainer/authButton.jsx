import React from 'react';
import * as Authentification from '../../containers/LoginPage/actions';
import { getObjectFromSessionStorage } from '../../services/localStorageService';
import { isSignedIn } from '../../services/authService';

import './styles.css';

export const AuthButton = () => {
    const welcomeText = "Welcome, ";
    const buttonText = "Sign out";
    const user = getObjectFromSessionStorage("user");
    return (
        <div className="authentification-info-container">
           { isSignedIn()
            ? <p>{welcomeText}{user.name}
                <img src={user.avatar} alt={user.name}  title={user.name} />
                <button onClick={() => { Authentification.signout() }}>
                {buttonText}
                </button>
            </p>
            : <p>You are not logged in.</p>
           }
        </div>
    );
}
