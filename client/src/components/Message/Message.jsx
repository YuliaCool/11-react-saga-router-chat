import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export class Message extends Component {

    getDeletedMessageId = () => {
        return this.props.onDelete(this.props.message.id);
    }

    getEditedMessageId = (messageText) => {
        return this.props.onEdit(this.props.message.id, messageText);
    }
    getLikedMessageId = () => {
        return this.props.onLike(this.props.message.id, this.props.currentUserId, 'like');
    }
    getDisLikedMessageId = () => {
        return this.props.onLike(this.props.message.id, this.props.currentUserId, 'dislike');
    }
    
    renderMessageFooter() { // TO DO on server and saga
        const message = this.props.message;
        const currentUserId = this.props.currentUserId;
        const isMine = message.userId === currentUserId;
        const isLikedButtonText = 'perfect!';
        const notLikedButtonText = 'like';
        const isDISLikedButtonText = 'awful!';
        const notDISLikedButtonText = 'dislike';

        const isLiked =  message.usersLiked.includes(currentUserId);
        const isDISLiked =  message.usersDisLiked.includes(currentUserId);
        return(
            <div className="message-footer">
            <div>
                 <span>{message.countLikes}</span>
                <span> likes </span>
            </div>
            { !isMine 
                ? <button 
                        className={"message-controls"} 
                        onClick={this.getLikedMessageId}> 
                            {isLiked ? isLikedButtonText : notLikedButtonText }
                  </button> 
                : <span className={"message-controls-between"}></span>
            }
            
            { !isMine 
                ? <button 
                        className={"message-controls"} 
                        onClick={this.getDisLikedMessageId}> 
                            {isDISLiked ? isDISLikedButtonText : notDISLikedButtonText }
                  </button> 
                : <span className={"message-controls-between"}></span>
            }
            <div>
                <span>  {message.countDisLikes}</span>
                <span> dislikes </span>
            </div>
        </div>
        );
    }

    render() { 
        const message = this.props.message;
        const currentUserId = this.props.currentUserId
        const isMine = message.userId === currentUserId;

        const localCreatedAt = new Date(message.createdAt).toLocaleString();
        const localEditedAt = message.editedAt 
                                ? new Date(message.editedAt).toLocaleString()
                                : null;

        return (
            <div className={`message-container ${isMine ? "my" : ""}`}>
                <div className="message-header">
                <div className="message-date">{localCreatedAt}</div>
                {   localEditedAt
                    ? <div className="message-date"> edited at {localEditedAt}</div>
                    : null
                }
                { isMine 
                    ?
                    <div className={'message-controls-hidden'}>
                        <button className={'message-controls'} onClick={this.getEditedMessageId}>edit</button>
                        <button className={'message-controls'} onClick={this.getDeletedMessageId}>delete</button>
                    </div>
                    : null
                }
                </div>
                <div className={"messageBody"}>
                    { !isMine 
                        ?  <div className="avatar">
                                <img src={ message.avatar } alt="user-avatar"/>
                            </div>
                        : null
                    }               
                    <div className="message-text">
                        <p>{message.text}</p>
                    </div>
                </div>
            </div>
        )
    }
};

Message.propTypes = {
    message: PropTypes.object,
    currentUserId: PropTypes.string,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func,
    onLike: PropTypes.func
};

export default Message;