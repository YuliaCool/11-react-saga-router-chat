import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PageHeader from './components/PageHeader/PageHeader';
import PageFooter from './components/PageFooter/PageFooter';
import { LoginPage } from './containers/LoginPage/index';
import UsersList from './containers/UsersList/index';
import UpdateMessagePage from './containers/UpdateMessagePage';
//import { PrivateRoute } from './containers/LoginPage/privateRoute';
import UserPage from './containers/UserPage';
import Chat from './containers/Chat';
import './App.css';


function App() {
  return (
    <div className="App">
      <header> 
        <PageHeader />
      </header>
      <div className="App-content">
      <Switch>
       
        
        {/* Authentification part: private route only for admin
        <Route path="/">
          <LoginPage/>
        </Route>
         <PrivateRoute path="/userslist">
            <UsersList />
        </PrivateRoute> */}
        <Route exact path="/login" component={ LoginPage } />
        <Route exact path="/userslist" component={UsersList} />
        <Route exact path="/users/:id" component={UserPage} />
        <Route exact path="/" component={ Chat } />
        <Route exact path="/chat" component={ Chat } />
        <Route exact path="/messages/:id" component={UpdateMessagePage} />
      </Switch>
      </div>
      <footer>
        <PageFooter />
      </footer>
    </div>
  );
}

export default App;
