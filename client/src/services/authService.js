import { post } from '../helpers/webApiHelper';
import { setSessionStorageItem, getObjectFromSessionStorage } from '../services/localStorageService';

const apiPath = "http://localhost:8080/api/auth/login";

export const login = async (body) => {
    const responce = await post(apiPath, body);
    return responce.json();
}

export const isSignedIn = () => {
    const user = getObjectFromSessionStorage('user');
    return user ? true : false;
};

export const setLoginSession = (user) => {
    setSessionStorageItem('user', user);
}

export const unsetLoginSession = () => {
    setSessionStorageItem('user', null);
}

