export const setSessionStorageItem = (key, val) => {
    const value = typeof val === 'object' ? JSON.stringify(val) : val;
    sessionStorage.setItem(key, value);
}

export const getObjectFromSessionStorage = (key) => {
    const data = sessionStorage.getItem(key);
    if(data) {
        return JSON.parse(data);
    }
    return null;
}