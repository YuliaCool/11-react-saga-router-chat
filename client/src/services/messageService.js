import { get } from '../helpers/webApiHelper';
const apiPath = "http://localhost:8080/api/messages";

export const getAllMessages = async () => {
    const responce = await get(apiPath);
    return responce.json();
}

export const getCountParticipants = (collectionMessages) => {
    const userIds = new Set();
    collectionMessages.forEach(message => {
        userIds.add(message.userId);
    });
    return userIds.size;
}