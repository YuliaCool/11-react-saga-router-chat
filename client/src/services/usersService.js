import { get } from '../helpers/webApiHelper';
const apiPath = "http://localhost:8080/api/users";

export const getAllUsers = async () => {
    const responce = await get(apiPath);
    return responce.json();
}
