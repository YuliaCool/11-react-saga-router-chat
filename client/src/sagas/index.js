import { all } from 'redux-saga/effects';
import usersSagas from '../containers/UsersList/sagas';
import userPageSagas from "../containers/UserPage/sagas";
import messagesSagas from "../containers/Chat/sagas";
import updateMessagePage from "../containers/UpdateMessagePage/sagas";

export default function* rootSaga() {
    yield all([
        usersSagas(),
        userPageSagas(),
        messagesSagas(),
        updateMessagePage()
    ])
};