const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        let messages = MessageService.getMessages();
        messages = MessageService.sortMessagesByCreatedDate(messages);
        res.data = messages;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const message = MessageService.getMessage(id);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', (req, res, next) => {
    try {
        const newMessageData = req.body;
        const message = MessageService.createMessage(newMessageData);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const updatedMessageData = req.body;
        const message = MessageService.updateMessage(id, updatedMessageData);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const message = MessageService.deleteMessage(id);
        res.data = message;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
