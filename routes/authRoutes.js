const { Router } = require('express');
const AuthService = require('../services/authService');
const checkUserValidData = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', checkUserValidData, (req, res, next) => {
    try {
        const { email, password } = req.body;
        let user = AuthService.login({ email, password });
        if(user){
            res.data = user;
        }  else {
            throw "Incorrect email or password";
        }
    } catch (err) {
        res.err = err;
        res.err.status = 404;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;