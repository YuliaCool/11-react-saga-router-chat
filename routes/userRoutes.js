const { Router } = require('express');
const UserService = require('../services/userService');
const checkUserValidData = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        const users = UserService.getUsers();
        res.data = users;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const user = UserService.getUser(id);
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', (req, res, next) => {
    try {
        const newUserData = req.body;
        const user = UserService.createUser(newUserData);
        res.data = user;

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', checkUserValidData, (req, res, next) => {
    try {
        const id = req.params.id;
        const updatedUserData = req.body;
        const user = UserService.updateUser(id, updatedUserData);
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const user = UserService.deleteUser(id);
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;